/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.mediatype;

import java.util.Map;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class MediaTypeTest {
    @Test
    void Restricted_names_should_be_case_insensitive() {
        assertEquals(
            new MediaType(
                "t",
                "s",
                Map.of("p", "v")
            ),
            new MediaType(
                "T",
                "S",
                Map.of("P", "v")
            )
        );
    }

    @Test
    void Parameter_values_should_be_case_sensitive() {
        assertNotEquals(
            new MediaType(
                "t",
                "s",
                Map.of("p", "v")
            ),
            new MediaType(
                "t",
                "s",
                Map.of("p", "V")
            )
        );
    }
}
