/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.mediatype;

import java.io.Serializable;
import static java.util.Collections.emptyMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import smartefact.util.Characters;
import static smartefact.util.Preconditions.require;
import static smartefact.util.Preconditions.requireNotEmpty;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Media type.
 *
 * @author Laurent Pireyn
 * @see <a href="https://datatracker.ietf.org/doc/html/rfc6838">RFC6838</a>
 * @see <a href="https://en.wikipedia.org/wiki/Media_type">Media type on Wikipedia</a>
 */
public final class MediaType implements Serializable {
    private static final char SLASH = '/';
    private static final char DOT = '.';
    private static final char PLUS = '+';
    private static final char SEMICOLON = ';';
    private static final char EQUAL = '=';
    private static final char DQUOTE = '"';
    private static final char BACKSLASH = '\\';
    private static final int RESTRICTED_NAME_MAX_LENGTH = 127;

    private static final long serialVersionUID = 1L;

    @Contract("_ -> new")
    public static @NotNull MediaType parseMediaType(@NotNull String string) {
        requireNotNull(string);
        final int slashIndex = string.indexOf(SLASH);
        require(
            slashIndex != -1,
            () -> "The string <" + string + "> is not a valid media type, as it does not contain a slash"
        );
        final String type = string.substring(0, slashIndex);
        int semicolonIndex = string.indexOf(SEMICOLON, slashIndex + 1);
        final String subtype;
        final Map<String, String> parameters;
        if (semicolonIndex == -1) {
            // No parameters
            subtype = string.substring(slashIndex + 1);
            // Optimization: avoid creating parameters map
            parameters = null;
        } else {
            // Parameter(s)
            subtype = string.substring(slashIndex + 1, semicolonIndex);
            parameters = new HashMap<>();
            while (semicolonIndex != -1) {
                final int equalIndex = string.indexOf(EQUAL, semicolonIndex + 1);
                require(
                    equalIndex != -1,
                    () -> "A parameter in the media type <" + string + "> is invalid, as it does not contain an equal sign"
                );
                final String name = string.substring(semicolonIndex + 1, equalIndex);
                final int nextSemicolonIndex = string.indexOf(SEMICOLON, equalIndex + 1);
                final String value = nextSemicolonIndex != -1
                    ? string.substring(equalIndex + 1, nextSemicolonIndex)
                    : string.substring(equalIndex + 1);
                // TODO: Handle quoted value
                parameters.put(name, value);
                semicolonIndex = nextSemicolonIndex;
            }
        }
        return new MediaType(
            type,
            subtype,
            parameters
        );
    }

    @Contract("_, _ -> param1")
    private static @NotNull String requireLegalRestrictedName(
        @NotNull String string,
        @NotNull String name
    ) {
        requireNotEmpty(string, () -> "The " + name + " <" + string + "> is null or empty");
        final int length = string.length();
        require(
            length <= RESTRICTED_NAME_MAX_LENGTH,
            () -> "The " + name + " <" + string + "> is longer than " + RESTRICTED_NAME_MAX_LENGTH + " characters"
        );
        require(
            isLegalRestrictedNameFirstChar(string.charAt(0)),
            () -> "The character #0 of the " + name + " <" + string + "> is illegal"
        );
        for (int i = 1; i < length; ++i) {
            // Final i to be used in lambda
            final int finalI = i;
            require(
                isLegalRestrictedNameChar(string.charAt(i)),
                () -> "The character #" + finalI + " of the " + name + " <" + string + "> is illegal"
            );
        }
        return string;
    }

    @Contract(pure = true)
    private static @NotNull String quotedIfNecessary(@NotNull String string) {
        final int length = string.length();
        // Build the quoted string while determining if quotes are necessary
        final StringBuilder quotedBuilder = new StringBuilder(1 + length + 1);
        quotedBuilder.append(DQUOTE);
        boolean quoted = false;
        for (int i = 0; i < length; ++i) {
            final char c = string.charAt(i);
            if (!isLegalRestrictedNameChar(c)) {
                if (c == DQUOTE || c == BACKSLASH) {
                    // TODO: It's not clear in the RFC how characters are escaped
                    quotedBuilder.append(BACKSLASH);
                }
                quoted = true;
            }
            quotedBuilder.append(c);
        }
        if (quoted) {
            // Quotes are necessary
            quotedBuilder.append(DQUOTE);
            return quotedBuilder.toString();
        }
        // Quotes are not necessary
        return string;
    }

    @Contract(pure = true)
    private static boolean isLegalRestrictedNameFirstChar(char c) {
        return Characters.isAlphanumeric(c);
    }

    @Contract(pure = true)
    private static boolean isLegalRestrictedNameChar(char c) {
        return Characters.isAlphanumeric(c)
            || c == '!'
            || c == '#'
            || c == '$'
            || c == '&'
            || c == '-'
            || c == '^'
            || c == '_'
            || c == DOT
            || c == PLUS;
    }

    private final @NotNull String type;
    private final @NotNull String subtype;
    private final @NotNull Map<String, String> parameters;
    private final int hashCode;
    private final @NotNull String string;

    public MediaType(
        @NotNull String type,
        @NotNull String subtype
    ) {
        this(
            type,
            subtype,
            null
        );
    }

    public MediaType(
        @NotNull String type,
        @NotNull String subtype,
        @Nullable Map<String, String> parameters
    ) {
        this.type = requireLegalRestrictedName(type, "type");
        this.subtype = requireLegalRestrictedName(subtype, "subtype");
        if (parameters != null && !parameters.isEmpty()) {
            this.parameters = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
            for (final Map.Entry<String, String> entry : parameters.entrySet()) {
                String name = entry.getKey();
                requireNotNull(name, "The name of a parameter is null");
                name = name.trim();
                requireLegalRestrictedName(name, "parameter name");
                String value = entry.getValue();
                requireNotNull(value, "The value of the parameter <" + name + "> is null");
                value = value.trim();
                // Final name to be used in lambda
                final String finalName = name;
                require(
                    this.parameters.put(name, value) == null,
                    () -> "The parameter <" + finalName + "> is duplicate"
                );
            }
        } else {
            this.parameters = emptyMap();
        }
        hashCode = Objects.hash(
            type,
            subtype,
            parameters
        );
        final StringBuilder stringBuilder = new StringBuilder(100);
        stringBuilder.append(type).append(SLASH).append(subtype);
        for (final Map.Entry<String, String> entry : this.parameters.entrySet()) {
            final String name = entry.getKey();
            final String value = entry.getValue();
            stringBuilder.append(SEMICOLON).append(' ')
                .append(name).append(EQUAL).append(quotedIfNecessary(value));
        }
        string = stringBuilder.toString();
    }

    @Contract(pure = true)
    public @NotNull String getType() {
        return type;
    }

    @Contract(pure = true)
    public @NotNull String getSubtype() {
        return subtype;
    }

    @Contract(pure = true)
    public @Nullable String getSuffix() {
        final int index = subtype.lastIndexOf(PLUS);
        return index != -1 ? subtype.substring(index + 1) : null;
    }

    @Contract(pure = true)
    public @NotNull Map<String, String> getParameters() {
        return parameters;
    }

    @Override
    @Contract(value = "null -> false", pure = true)
    public boolean equals(@Nullable Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof MediaType)) {
            return false;
        }
        final MediaType other = (MediaType) object;
        return type.equalsIgnoreCase(other.type)
            && subtype.equalsIgnoreCase(other.subtype)
            && parameters.equals(other.parameters);
    }

    @Override
    @Contract(pure = true)
    public int hashCode() {
        return hashCode;
    }

    @Override
    @Contract(pure = true)
    public String toString() {
        return string;
    }
}
