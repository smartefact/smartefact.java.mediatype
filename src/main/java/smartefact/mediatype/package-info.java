/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Java library to work with media types.
 *
 * @author Laurent Pireyn
 * @see smartefact.mediatype.MediaType
 * @see <a href="https://datatracker.ietf.org/doc/html/rfc6838">RFC6838</a>
 * @see <a href="https://en.wikipedia.org/wiki/Media_type">Media type on Wikipedia</a>
 */
package smartefact.mediatype;
