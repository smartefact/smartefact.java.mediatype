/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.mediatype;

/**
 * Common {@link MediaType}s and related constants.
 *
 * @author Laurent Pireyn
 */
public final class MediaTypes {
    public static final String TYPE_APPLICATION = "application";
    public static final String TYPE_AUDIO = "audio";
    public static final String TYPE_EXAMPLE = "example";
    public static final String TYPE_FONT = "font";
    public static final String TYPE_IMAGE = "image";
    public static final String TYPE_MESSAGE = "message";
    public static final String TYPE_MODEL = "model";
    public static final String TYPE_MULTIPART = "multipart";
    public static final String TYPE_TEXT = "text";
    public static final String TYPE_VIDEO = "video";

    public static final String PARAM_CHARSET = "charset";

    public static final MediaType APPLICATION_GRAPHQL = new MediaType(TYPE_APPLICATION, "graphql");
    public static final MediaType APPLICATION_JAVASCRIPT = new MediaType(TYPE_APPLICATION, "javascript");
    public static final MediaType APPLICATION_JSON = new MediaType(TYPE_APPLICATION, "json");
    public static final MediaType APPLICATION_PDF = new MediaType(TYPE_APPLICATION, "pdf");
    public static final MediaType APPLICATION_SQL = new MediaType(TYPE_APPLICATION, "sql");
    public static final MediaType APPLICATION_X_WWW_FORM_URLENCODED = new MediaType(TYPE_APPLICATION, "x-www-form-urlencoded");
    public static final MediaType APPLICATION_X_YAML = new MediaType(TYPE_APPLICATION, "x-yaml");
    public static final MediaType APPLICATION_XML = new MediaType(TYPE_APPLICATION, "xml");
    public static final MediaType APPLICATION_ZIP = new MediaType(TYPE_APPLICATION, "zip");

    public static final MediaType AUDIO_MPEG = new MediaType(TYPE_AUDIO, "mpeg");
    public static final MediaType AUDIO_OGG = new MediaType(TYPE_AUDIO, "ogg");

    public static final MediaType IMAGE_GIF = new MediaType(TYPE_IMAGE, "gif");
    public static final MediaType IMAGE_JPEG = new MediaType(TYPE_IMAGE, "jpeg");
    public static final MediaType IMAGE_PNG = new MediaType(TYPE_IMAGE, "png");
    public static final MediaType IMAGE_SVG = new MediaType(TYPE_IMAGE, "svg+xml");
    public static final MediaType IMAGE_WEBP = new MediaType(TYPE_IMAGE, "webp");

    public static final MediaType MULTIPART_FORM_DATA = new MediaType(TYPE_IMAGE, "form-data");

    public static final MediaType TEXT_CSS = new MediaType(TYPE_TEXT, "css");
    public static final MediaType TEXT_CSV = new MediaType(TYPE_TEXT, "csv");
    public static final MediaType TEXT_HTML = new MediaType(TYPE_TEXT, "html");
    public static final MediaType TEXT_PLAIN = new MediaType(TYPE_TEXT, "plain");
    public static final MediaType TEXT_XML = new MediaType(TYPE_TEXT, "xml");
    public static final MediaType TEXT_YAML = new MediaType(TYPE_TEXT, "yaml");

    private MediaTypes() {}
}
