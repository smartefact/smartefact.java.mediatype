[![Latest release](https://gitlab.com/smartefact/smartefact.java.mediatype/-/badges/release.svg?key_text=Latest%20release&key_width=100)](https://gitlab.com/smartefact/smartefact.java.mediatype/-/releases)
[![Pipeline status](https://gitlab.com/smartefact/smartefact.java.mediatype/badges/main/pipeline.svg?key_text=Pipeline%20status&key_width=100)](https://gitlab.com/smartefact/smartefact.java.mediatype/-/commits/main)
[![Coverage](https://gitlab.com/smartefact/smartefact.java.mediatype/badges/main/coverage.svg?key_text=Coverage&key_width=100)](https://gitlab.com/smartefact/smartefact.java.mediatype/-/commits/main)

# smartefact.java.mediatype

Java library to work with [media types](https://en.wikipedia.org/wiki/Media_type).

## Changelog

See [CHANGELOG](CHANGELOG.md).

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md).

## License

This project is licensed under the [Apache License 2.0](LICENSE).
